import numpy as np
import pandas as pd
import scipy.stats as stats
from pathlib import Path
import glob
import pickle
import random
import os
import matplotlib.pyplot as plt
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler, LabelEncoder, MinMaxScaler
import tensorflow as tf
import tensorflow.keras.layers as L
import tensorflow.keras.models as M
import tensorflow.keras.backend as K
from tensorflow.keras.callbacks import ReduceLROnPlateau, ModelCheckpoint, EarlyStopping

def comp_metric_waypoint(xhat, yhat, x, y):
    intermediate = np.sqrt(np.power(xhat-x, 2) + np.power(yhat-y, 2))
    return intermediate.sum()/xhat.shape[0]
def comp_metric_floor(fhat, f):
    intermediate = np.abs(fhat-f)
    return intermediate.sum()/fhat.shape[0]

feature_dir = f"/nobackup/ml19gy/archive"
train_files = sorted(glob.glob(os.path.join(feature_dir, '*_train.csv')))
test_files = sorted(glob.glob(os.path.join(feature_dir, '*_test.csv')))
subm = pd.read_csv(f'/nobackup/ml19gy/sample_submission.csv', index_col=0)
data = pd.read_csv('/nobackup/ml19gy/archive/train_all.csv')
test_data = pd.read_csv('/nobackup/ml19gy/archive/test_all.csv')

BSSID_FEATS = [f'bssid_{i}' for i in range(100)]
RSSI_FEATS  = [f'rssi_{i}' for i in range(100)]
wifi_bssids = []
for i in range(100):
    wifi_bssids.extend(data.iloc[:,i].values.tolist())
wifi_bssids = list(set(wifi_bssids))
wifi_bssids_size = len(wifi_bssids)
wifi_bssids_test = []
for i in range(100):
    wifi_bssids_test.extend(test_data.iloc[:,i].values.tolist())
wifi_bssids_test = list(set(wifi_bssids_test))

wifi_bssids_size = len(wifi_bssids_test)
wifi_bssids.extend(wifi_bssids_test)
wifi_bssids_size = len(wifi_bssids)

label = LabelEncoder()
label.fit(wifi_bssids)
label_site = LabelEncoder()
label_site.fit(data['site_id'])
rssi_scaler = MinMaxScaler()
rssi_scaler.fit(data.loc[:,RSSI_FEATS])

data.loc[:,RSSI_FEATS] = rssi_scaler.transform(data.loc[:,RSSI_FEATS])

for i in BSSID_FEATS:
    data.loc[:,i] = label.transform(data.loc[:,i])
    data.loc[:,i] = data.loc[:,i] + 1
    
data.loc[:, 'site_id'] = label_site.transform(data.loc[:, 'site_id'])

test_data.loc[:,RSSI_FEATS] = rssi_scaler.transform(test_data.loc[:,RSSI_FEATS])

for i in BSSID_FEATS:
    test_data.loc[:,i] = label.transform(test_data.loc[:,i])
    test_data.loc[:,i] = test_data.loc[:,i] + 1
    
test_data.loc[:, 'site_id'] = label_site.transform(test_data.loc[:, 'site_id'])

site_count = len(data['site_id'].unique())
data.reset_index(drop=True, inplace=True)

def create_model(input_data):

    # bssid
    input_dim = input_data[0].shape[1]
    input_embd_layer = L.Input(shape=(input_dim,))
    x1 = L.Embedding(wifi_bssids_size,64)(input_embd_layer)
    x1 = L.Flatten()(x1)

    # rssi
    input_dim = input_data[1].shape[1]
    input_layer = L.Input(input_dim, )
    x2 = L.BatchNormalization()(input_layer)
    x2 = L.Dense(256, activation='elu')(x2)

    # site
    input_site_layer = L.Input(shape=(1,))
    x3 = L.Embedding(site_count, 2)(input_site_layer)
    x3 = L.Flatten()(x3)

    x = L.Concatenate(axis=1)([x1, x3, x2])
    x = L.BatchNormalization()(x)
    x = L.Dropout(0.3)(x)
    x = L.Dense(128, activation='elu')(x)
    x = L.Reshape((1, -1))(x)
    
    #LSTM
    x = L.BatchNormalization()(x)
    x = L.LSTM(64, dropout=0.3, recurrent_dropout=0.3, return_sequences=True)(x)
    x = L.LSTM(32, dropout=0.3, recurrent_dropout=0.3, return_sequences=False)(x)

    output_layer_1 = L.Dense(2, name='xy')(x)
    output_layer_2 = L.Dense(1, activation='softmax', name='floor')(x)
  
    model = M.Model([input_embd_layer, input_layer, input_site_layer], 
                    [output_layer_1, output_layer_2])

    model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.003),
                  loss='mse', metrics=['mse'])

    return model

site_x, site_y, site_f = np.zeros(data.shape[0]), np.zeros(data.shape[0]), np.zeros(data.shape[0])

for fold, (trn_idx, val_idx) in enumerate(StratifiedKFold(n_splits=10, shuffle=True).split(data.loc[:, 'path'], data.loc[:, 'path'])):
    X_train = data.loc[trn_idx, BSSID_FEATS + RSSI_FEATS + ['site_id']]
    y_trainx = data.loc[trn_idx, 'x']
    y_trainy = data.loc[trn_idx, 'y']
    y_trainf = data.loc[trn_idx, 'floor']

    waypoint = pd.concat([y_trainx, y_trainy], axis=1)
    
    y_train = [waypoint, y_trainf]

    X_valid = data.loc[val_idx, BSSID_FEATS + RSSI_FEATS + ['site_id']]
    y_validx = data.loc[val_idx, 'x']
    y_validy = data.loc[val_idx, 'y']
    y_validf = data.loc[val_idx, 'floor']

    waypoint = pd.concat([y_validx, y_validy], axis=1)
    y_valid = [waypoint, y_validf]

    model = create_model([X_train.loc[:,BSSID_FEATS], X_train.loc[:,RSSI_FEATS], X_train.loc[:,'site_id']])
    
    history = model.fit([X_train.loc[:,BSSID_FEATS], X_train.loc[:,RSSI_FEATS], X_train.loc[:,'site_id']], y_train, 
                validation_data=([X_valid.loc[:,BSSID_FEATS], X_valid.loc[:,RSSI_FEATS], X_valid.loc[:,'site_id']], y_valid), 
                batch_size=256, epochs=100,
                callbacks=[
                ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=3, verbose=1, min_delta=1e-4, mode='min')
                , ModelCheckpoint(f'/home/home02/ml19gy/job/job1/RNN_1_{fold}.hdf5', monitor='val_loss', verbose=0, save_best_only=True, save_weights_only=True, mode='min')
                , EarlyStopping(monitor='val_loss', min_delta=1e-4, patience=5, mode='min', baseline=None, restore_best_weights=True)
            ])
    
    model.load_weights(f'/home/home02/ml19gy/job/job1/RNN_1_{fold}.hdf5')

    val_pred = model.predict([X_valid.loc[:,BSSID_FEATS], X_valid.loc[:,RSSI_FEATS], X_valid.loc[:,'site_id']])

    site_x[val_idx] = val_pred[0][:,0]
    site_y[val_idx] = val_pred[0][:,1]
    site_f[val_idx] = val_pred[1][:,0].astype(int)

    score_waypoint = comp_metric_waypoint(site_x[val_idx], site_y[val_idx],
                        y_validx.to_numpy(), y_validy.to_numpy())
    score_floor = comp_metric_floor(site_f[val_idx], y_validf.to_numpy())     

    print(f"fold {fold}: mean waypoint position error {score_waypoint}")
    print(f"fold {fold}: mean floor position error {score_floor}")

print("~"*50)
score_waypoint = comp_metric_waypoint(site_x, site_y,data.iloc[:, -5].to_numpy(), data.iloc[:, -4].to_numpy())
score_floor = comp_metric_floor(site_f, data.iloc[:, -3].to_numpy())
print(f"mean waypoint position error {score_waypoint}")
print(f"mean floor position error {score_floor}")
print("~"*50)

